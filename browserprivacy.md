# Browser Privacy
## What Browser Do I Choose?
There are heaps of browsers to sift through, and there's a couple which respect your privacy, some that only say they do, but are lying and some which do not at all.

Do Not Use | OK | Use
------ | ------ | ------
Google Chrome🔷 | Firefox🔶 | GNU IceCat🔶
Internet Explorer | Brave🔷 | Lynx
Microsoft Edge🔷 | Vivaldi🔷 | Pale Moon🔶
Opera🔷 | Chromium🔷 | Ungoogled Chromium🔷
Waterfox🔶 | Dissenter🔷 | Librefox🔶
Safari | SeaMonkey | Tor Browser🔶
SRWare Iron🔷 | / | Iridium Browser🔷
Yandex🔷 | / | Falkon

**🔷 means Chromium-based and 🔶 means Firefox-based**

**Google Chrome**
https://www.google.com/chrome/
https://www.google.com/chrome/privacy/
> This browser is the most known for violating your privacy, and it has no shame in hiding it.

**Firefox**
https://www.mozilla.org/en-US/privacy/firefox/
